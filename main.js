// Kristijan Jakimov
// Da die Aufgabe auf Englisch geschrieben ist, werde ich meine Notizen auf Englisch schreiben



// class from which I will be creating objects in order to be added into the rendering array
class CreateNewNoteCard {
    constructor (title, notes ) {
        this.title = title;
        this.notes = notes;
    }
    id = new Date().valueOf();
}
// container where I will render the notes at
const mainContainer = document.querySelector('.main-grid');

// search area in my nav-bar variables
const searchInput = document.querySelector('.search-input');
const searchButton = document.querySelector('.general-btn');

// add new/edit area variables
const addNewNotePlusSign = document.querySelector('.plus-sign');
const addNewNoteInputSpecifications = document.querySelector('#new-note-specs')
const noteTitleInput = document.querySelector('#note-title')
const noteBodyInput = document.querySelector('#note-body')
const addMyNoteBtn = document.querySelector('#addMyNoteBtn')
addMyNoteBtn.innerText = 'Add New Note';
// array that I will work with
let myCurrentNotesArray = [];
// declaring a global variable that will save the ID of the edited note
let editedID;
// creating a note card 
addNewNotePlusSign.addEventListener('click', function(){
// display the input fields where the user can write his notes at
    addNewNoteInputSpecifications.classList.toggle('display-none');
})

addMyNoteBtn.addEventListener('click', function () {
    // create a new object from the values from the inputs
        let newCard = new CreateNewNoteCard (noteTitleInput.value, noteBodyInput.value);
    // check if it is in adding new item mode. Because on 138 line starts the Edit item logic
    if (addMyNoteBtn.innerText === 'Add New Note') {
            myCurrentNotesArray.unshift(newCard);
        // clear the html of the body in case this was an edit
            mainContainer.innerHTML = '';

    }
    if (addMyNoteBtn.innerText === 'Save') {
        // do the same as delete
        myCurrentNotesArray = myCurrentNotesArray.filter(element => element.id != editedID)
        myCurrentNotesArray.unshift(newCard);
        // set the button back to add
        addMyNoteBtn.innerText = 'Add New Note';

    }
    // call an array that renders
    renderTheArray(myCurrentNotesArray);
    // clean up
    noteTitleInput.value = '';
    noteBodyInput.value = '';
    addNewNoteInputSpecifications.classList.toggle('display-none');
})

// define the rendering function
function renderTheArray(array) {
    mainContainer.innerHTML = '';
// here i will iterate through the array of objects and crete a card for each object that is selected
array.forEach(element => {

// create the note
    const note = document.createElement('div');
    note.className = 'grid-item';
    note.id = element.id;


    const noteInner = document.createElement('div');
    noteInner.className = 'grid-item-inner';
note.appendChild(noteInner)
    const h3 = document.createElement('h3');
    h3.textContent = element.title;

    const hr1 = document.createElement('hr');
    hr1.className = 'display-none-withinTheCard';

    const p = document.createElement('p');
    p.className = 'display-none-withinTheCard';
    p.textContent = element.notes;

    const hr2 = document.createElement('hr');
    hr2.className = 'display-none-withinTheCard';

    const buttonsDiv = document.createElement('div');
    buttonsDiv.className = 'buttons-div';

    const readButton = document.createElement('button');
    readButton.className = 'btn action-btn btn-read';
    readButton.textContent = 'Read';

    const editButton = document.createElement('button');
    editButton.className = 'btn action-btn mx-1 btn-edit';
    editButton.textContent = 'Edit';

    const deleteButton = document.createElement('button');
    deleteButton.className = 'btn btn-danger';
    deleteButton.textContent = 'Delete';

    buttonsDiv.appendChild(readButton);
    buttonsDiv.appendChild(editButton);
    buttonsDiv.appendChild(deleteButton);

    noteInner.appendChild(h3);
    noteInner.appendChild(hr1);
    noteInner.appendChild(p);
    noteInner.appendChild(hr2);
    noteInner.appendChild(buttonsDiv);

    mainContainer.appendChild(note);
    // select each action button and add an even listener for each and every one of them. 
    // I do this this way because it is easier for me to get the Id of the element, 
    // in order to edit or delete
    // function createCard (element) {}

    // read will toggle the body part of the note to be either seen or not
readButton.addEventListener('click', function() {
    p.classList.toggle('display-none-withinTheCard');
    hr1.classList.toggle('display-none-withinTheCard');
    hr2.classList.toggle('display-none-withinTheCard');
})

// delete will delete the card, but also remove the object from the array and re-render the main contaier

deleteButton.addEventListener('click', function () {
note.remove()
myCurrentNotesArray = array.filter(objectElement => objectElement.id != element.id)

renderTheArray(myCurrentNotesArray)
})

// on click, show the add new area but update it with the note details. Change the inner text of the
// button to 'edit' so I can distinguish if I am editing or adding
editButton.addEventListener('click', function () {
    addMyNoteBtn.innerText = 'Save';
    addNewNoteInputSpecifications.classList.toggle('display-none');
    noteTitleInput.value = element.title;
    noteBodyInput.value = element.notes;
    editedID = element.id;
})
});
}


// while typing the Input, check if any part of the written value matches any of the note titles
searchInput.addEventListener('keydown', function (e) {
    // render the original array if it is empty
        if (searchInput.value === '') {
            renderTheArray(myCurrentNotesArray)
        }
})

// by pressing the search butten check if any title fully matches the value
searchButton.addEventListener('click', function(){
    if (searchInput.value === '') {
        renderTheArray(myCurrentNotesArray)
    }
    else {
        let filteredArray = [];
        filteredArray = myCurrentNotesArray.filter(element =>
            element.title.includes(searchInput.value)  
            )
    
    renderTheArray(filteredArray)
    if (filteredArray.length == 0) {
        mainContainer.innerText = 'No note was found for your search keyword/s'
    }
    }
    })
